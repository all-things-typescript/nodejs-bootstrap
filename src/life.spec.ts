import { calculateTheMeaningOfLife } from './life';

describe('What is Life anyways?', () => {
  it('should tell the Meaning of Life', () => {
    const result = calculateTheMeaningOfLife();
    expect(result).toBe(42);
  });
});
