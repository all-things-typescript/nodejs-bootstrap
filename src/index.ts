import { calculateTheMeaningOfLife } from './life';

const main = async () => {
  console.log('What is the meaning of Life?');

  const handle = setInterval(() => {
    console.log(calculateTheMeaningOfLife());
  }, 1000);

  process.on('SIGINT', () => {
    console.log('Life has come to an end!');
    clearInterval(handle);
  });
};

main();
