# nodejs-typescript-bootstrap

>Barebone NodeJS Bootstrap Project with sane defaults to get things started without doing the chore over and over again. :/

## Developing

### Prerequisites

- NodeJS ver. 10.15.0+ (Use nvm!)
- [Node Version Manager](https://github.com/creationix/nvm)
- [Docker-CE](https://docs.docker.com/install/) & [Docker-Compose](https://docs.docker.com/compose/install/)

### Installing / Getting started

```bash
# Activate NodeJS Version defined within the .nvmrc file
$ nvm use

# or

$ nvm use 10.15.0
```

```bash
# Install default dependencies
$ yarn install
```

### Debugging

>This project contains a VSCode launch configuration which allows to execute the Typescript files directly via the Debugger. Just put a Breakpoint where you want and press F5.

### Building

#### Local

```bash
# Build application in development mode
$ yarn build
```

```bash
# Build application in production mode
$ yarn build:prod
```

#### Docker

```bash
# Build the Docker image & start the container / application
$ make up
```

```bash
# Rebuild the Docker image & start the container / application from ground up
$ make rebuild
```

### Linting & Testing
```bash
# Lint the applications source
$ yarn lint
```

```bash
# Run all test solution(s)
$ yarn test
```

```bash
# Continuously run all test solution(s)
$ yarn test:watch
```

### Executing

#### Local

```bash
# Execute and build the application via ts-node
$ yarn start:dev
```

#### Docker

```bash
# Starts the Docker container
$ make start
```

```bash
# Stops the Docker container
$ make stop
```

### Shell & Logs

#### Docker

```bash
# Shows the stdout of the Docker container
$ make logs
```

```bash
# Connects to the Docker containers bash shell
$ make bash

# or

$ make shell

# You leave the bash shell with
$ exit
```

## Links

### Useful Tools

- [Node Version Manager](https://github.com/creationix/nvm)

### Code & Styleguides

- [clean-code-typescript](https://labs42io.github.io/clean-code-typescript/)
- [Typescript Styleguard](https://basarat.gitbooks.io/typescript/docs/styleguide/styleguide.html)

### Best Practices

- [Absolut best practices](https://github.com/i0natan/nodebestpractices)
- [Performance](https://www.tutorialdocs.com/article/nodejs-performance.html)
- [Write it Portable!](https://github.com/ehmicky/portable-node-guide)

### Tutorials

- [from-beginners-to-expert](https://nodefunction.com/nodejs/node-js-roadmap-a-route-from-beginners-to-become-expert-developer/)
- [containerizing](https://nodesource.com/blog/containerizing-node-js-applications-with-docker)
- [JS Info](http://javascript.info/)
- [Docker / NodeJS Healthcheck](https://anthonymineo.com/docker-healthcheck-for-your-node-js-app/)

### Awesome Lists

- [Pure Awesome List](https://github.com/sindresorhus/awesome-nodejs)

### Licenses

- [choosealicense](https://choosealicense.com/)

### Newsletter

- [nodeweekly](https://nodeweekly.com/)

## Author

>(c) Jan Schulte
>
>Date: 2019-02-05 20:15:58